from email import message
import pymysql
from app import app
from config import mysql
from flask import jsonify
from flask import flash, request

@app.route('/user')
def user():
    try:
        conn = mysql.connect()
        cursor = conn.cursor(pymysql.cursors.DictCursor)
        cursor.execute("SELECT * FROM user")
        rows = cursor.fetchall()
        res = jsonify(
            message="Berhasil Get Data",
            data=rows
            )
        res.status_code = 200
        return res
    except Exception as e:
        res = jsonify(message=e)
        res.status_code = 500
        return res
    finally:
        cursor.close() 
        conn.close()

@app.route('/history_topup')
def topuphistory():
    try:
        conn = mysql.connect()
        cursor = conn.cursor(pymysql.cursors.DictCursor)
        cursor.execute("SELECT topup_history.id,jumlah,user.nama FROM topup_history INNER JOIN user ON topup_history.id_user=user.id")
        rows = cursor.fetchall()
        res = jsonify(
            message="Berhasil Get Data",
            data=rows
            )
        res.status_code = 200
        return res
    except Exception as e:
        res = jsonify(message=e)
        res.status_code = 500
        return res
    finally:
        cursor.close() 
        conn.close()

@app.route('/history_transaksi')
def transaksi_history():
    try:
        conn = mysql.connect()
        cursor = conn.cursor(pymysql.cursors.DictCursor)
        cursor.execute("SELECT transaksi_history.id,user1.nama AS nama_pengirim,user.nama AS nama_penerima,jumlah FROM transaksi_history LEFT JOIN user AS user1 ON transaksi_history.id_pengirim = user1.id LEFT JOIN user ON transaksi_history.id_penerima = user.id")
        rows = cursor.fetchall()
        res = jsonify(
            message="Berhasil Get Data",
            data=rows
            )
        res.status_code = 200
        return res
    except Exception as e:
        res = jsonify(message=e)
        res.status_code = 500
        return res
    finally:
        cursor.close() 
        conn.close()

@app.route('/create-user', methods=['POST'])
def create():
    try:
        nama = request.form.get("nama")
        identitas = request.form.get("identitas")
        sql = "INSERT into user values(null,%s,%s,0)"
        data = (nama,identitas)
        conn = mysql.connect()
        cursor = conn.cursor()
        cursor.execute(sql, data)
        conn.commit()
        res = jsonify(message="Berhasil Save Data")
        res.status_code = 200
        return res
    except Exception as e:
        res = jsonify(message="Terjadi Kesalah")
        res.status_code = 500
        return res
    finally:
        cursor.close() 
        conn.close()


@app.route('/topup-user', methods=['POST'])
def topup():
    try:
        jumlah = request.form.get("jumlah")
        id_user = request.form.get("id_user")
        sql = "UPDATE user SET saldo = saldo + %s WHERE id = %s"
        sql2 = "INSERT into topup_history values(null,%s,%s)"
        data = (jumlah,id_user)
        data2 = (id_user,jumlah)
        conn = mysql.connect()
        cursor = conn.cursor()
        cursor.execute(sql, data)
        cursor.execute(sql2, data2)
        conn.commit()
        res = jsonify(message="Berhasil Top Up sebesar : "+jumlah)
        res.status_code = 200
        return res
    except Exception as e:
        res = jsonify(message="Terjadi Kesalahan")
        res.status_code = 500
        return res
    finally:
        cursor.close() 
        conn.close()
        
@app.route('/transaksi-user', methods=['POST'])
def transaksi():
    try:
        id_pengirim = request.form.get("id_pengirim")
        id_penerima = request.form.get("id_penerima")
        jumlah = request.form.get("jumlah")
        sql = "UPDATE user SET saldo = saldo + %s WHERE id = %s"
        sql2 = "UPDATE user SET saldo = saldo - %s WHERE id = %s"
        sql3 = "INSERT into transaksi_history values(null,%s,%s,%s)"
        data = (jumlah,id_penerima)
        data2 = (jumlah,id_pengirim)
        data3 = (id_pengirim,id_penerima,jumlah)
        conn = mysql.connect()
        cursor = conn.cursor()
        cursor.execute(sql, data)
        cursor.execute(sql2, data2)
        cursor.execute(sql3, data3)
        conn.commit()
        res = jsonify(message="Berhasil Mengirim Saldo Sebesar : "+jumlah)
        res.status_code = 200
        return res
    except Exception as e:
        res = jsonify(message="Terjadi Kesalahan")
        res.status_code = 500
        return res
    finally:
        cursor.close() 
        conn.close()

if __name__ == "__main__":
    app.run()	