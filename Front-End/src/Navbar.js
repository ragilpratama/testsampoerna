import React, { Component } from 'react';
import { NavItem } from 'react-bootstrap';
import Nav from 'react-bootstrap/Nav';
import { NavLink } from "react-router-dom";
import './App.css';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
} from "react-router-dom";

export class Navbar extends Component {

    render() {
        return(
                <Nav bg="light" className="justify-content-center navnya" >
                    <NavItem>
                    <NavLink className="btn btn-primary" to="/userlist">User List</NavLink>&nbsp;
                    </NavItem>
                    <NavItem>
                    <NavLink className="btn btn-primary" to="/userhistory">History Transfer</NavLink>&nbsp;
                    </NavItem>
                    <NavItem>
                    <NavLink className="btn btn-primary" to="/historytopup">History Topup</NavLink>&nbsp;
                    </NavItem>
                </Nav>
        )
    }

}

export default Navbar;