import React, { Component } from 'react';
import { Container } from 'react-bootstrap';
import Table from 'react-bootstrap/Table';
import axios from 'axios';

const baseURL = "http://127.0.0.1:5000/history_topup";

export class HistoryTopup extends Component {

    state = {
        users: []
        }
        
        componentDidMount() {
        axios.get(baseURL)
        .then(res => {
            const users = res.data.data;
            this.setState({ users });
            })
        }

    render() {
        return(
            <Container>
                <Table striped bordered hover>
            <thead>
                <tr>
                <th>ID Transaksi</th>
                <th>Nama</th>
                <th>Jumlah Topup</th>
                </tr>
            </thead>
            <tbody>
                { this.state.users.map(users => 
                    <tr>
                    <td>{users.id}</td>
                    <td>{users.nama}</td>
                    <td>{users.jumlah}</td>
                    </tr>
                )}
            </tbody>
            </Table>
            </Container>
        )
    }

}

export default HistoryTopup;