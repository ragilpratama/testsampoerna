import logo from './logo.svg';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import Button from 'react-bootstrap/Button';
import Navbar from './Navbar';
import UserList from './UserList';
import UserHistory from './UserHistory';
import {
  BrowserRouter as Router,
  Route,
  Routes,
  Link
} from "react-router-dom";
import HistoryTopup from './HistoryTopup';

function App() {
  return (
    <>
    <Router>
      <Navbar /> 
          <Routes>
            <Route path="/" element={<UserList />}></Route>
          </Routes>
          <Routes>
            <Route path='/userlist' element={<UserList />}></Route>
          </Routes>
          <Routes>
            <Route path='/userhistory' element={<UserHistory />}></Route>
          </Routes>
          <Routes>
            <Route path='/historytopup' element={<HistoryTopup />}></Route>
          </Routes>
    </Router>
    </>
  );
}

export default App;
