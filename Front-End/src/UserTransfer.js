import React, { Component } from 'react';
import { Container } from 'react-bootstrap';
import Table from 'react-bootstrap/Table';


export class UserTransfer extends Component {

    render() {
        return(
            <Container>
                <Table striped bordered hover>
            <thead>
                <tr>
                <th>#</th>
                <th>Transfer</th>
                <th>Last Name</th>
                <th>Username</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                <td>1</td>
                <td>Mark</td>
                <td>Otto</td>
                <td>@mdo</td>
                </tr>
                <tr>
                <td>2</td>
                <td>Jacob</td>
                <td>Thornton</td>
                <td>@fat</td>
                </tr>
            </tbody>
            </Table>
            </Container>
        )
    }

}

export default UserTransfer;