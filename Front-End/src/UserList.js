import React, { Component , useEffect , useState} from 'react';
import { Container } from 'react-bootstrap';
import Table from 'react-bootstrap/Table';
import axios from 'axios';

const baseURL = "http://127.0.0.1:5000/user";


export class UserList extends Component {
    state = {
        users: []
        }
        
        componentDidMount() {
        axios.get(baseURL)
        .then(res => {
            const users = res.data.data;
            this.setState({ users });
            })
        }
    render() {
        return(
            <Container>
            <Table striped bordered hover>
            <thead>
                <tr>
                <th>ID</th>
                <th>Nama</th>
                <th>Identitas</th>
                <th>Saldo</th>
                </tr>
            </thead>
            <tbody>
                { this.state.users.map(users => 
                    <tr>
                    <td key={users.id}>{users.id}</td>
                    <td>{users.nama}</td>
                    <td>{users.identitas}</td>
                    <td>{users.saldo}</td>
                    </tr>
                )}
            </tbody>
            </Table>
            </Container>
        )
    }

}

export default UserList;